import unittest
from chiffrementCesar import *

class TestchiffrementCesar(unittest.TestCase):

    def test_decoupage(self):
        self.assertEqual(decoupage("bonjour"), ['b','o','n','j','o','u','r',])
        self.assertEqual(decoupage(31582),['3','1','5','8','2',])

    def test_chiffrement(self):
        self.assertEqual(chiffrement(['b','o','n','j','o','u','r',],8),"jwvrw}z")
        self.assertEqual(chiffrement(['c','o','r','d',' ',')',']',],3),"frug#,`")

    def test_dechiffrement(self):
        self.assertEqual(dechiffrement(['b','o','n','j','o','u','r',],8),"Zgfbgmj")
        self.assertEqual(dechiffrement(['f','r','u','g','#',',','`',],3),"cord )]")

    def test_chiffrement_circulaire(self):
        self.assertEqual(chiffrement_circulaire(['b','o','n','j','o','u','r',],8),"jwvrwcz")
        self.assertEqual(chiffrement_circulaire(['c','o','r','d',' ',')',']',],3),"frug )]")

    def test_dechiffrement_circulaire(self):
        self.assertEqual(dechiffrement_circulaire(['b','o','n','j','o','u','r',],8),"tgfbgmj")
        self.assertEqual(dechiffrement_circulaire(['f','r','u','g','#',',','`',],3),"cord#,`")