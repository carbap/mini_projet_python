from chiffrementCesar import *
from tkinter import *


frame = Tk()

#Création de la grille 
frame.rowconfigure(1, weight=1)
frame.columnconfigure(1, weight=1)

#Création de la zone de saisie du pas via un label 
#Mise en place de la position de la zone de saisie 
label_step = Label(frame, text="Saisir le pas de chiffrement")
label_step.grid(row="0", column="0", columnspan="4")

#Attribution de la valeur entrée pour la pas de chiffrement à la variable step 
step = Entry(frame, textvariable=IntVar())
step.grid(row="1", column="0", columnspan="4")


#Création de la zone de saisie du texte via un label 
#Mise en place de la position de la zone de saisie 
label_saisie = Label(frame, text="Saisir le texte à chiffrer ou déchiffrer")
label_saisie.grid(row="2", column="0", columnspan="4")

#Création de la variable liant les radiobuttons
radioVar= IntVar()

#Création du radiobutton circulaire  
#Mise en place de la position du radiobutton 
radio_circulaire = Radiobutton(frame, variable=radioVar, text="Circulaire", value=1)
radio_circulaire.grid(row="4", column="1")


#Création du radiobutton circulaire  
#Mise en place de la position du radiobutton
radio_noncirculaire = Radiobutton(frame, variable=radioVar, text="Non circulaire", value=0)
radio_noncirculaire.grid(row="4", column="0")

#Attribution de la string entrée dans la zone de saisie à la variable entry 
entry = Text(frame)
entry.grid(row="3", column="0", columnspan="4")


#Affichage de la chaine de caractère obtenue après le chiffrement ou déchiffrement
#Mise en place de la position de cette zone de texte 
text_resultat = Text(frame)
text_resultat.grid(row="5", column="0", columnspan="4")

#Chiffrement de la chaine de caractère entrrée via le bouton chiffrement 
#De façon circulaire ou non via les radiobuttons 
def bouton_chiffrement():
    texte = decoupage(entry.get("0.0", CURRENT))
    if(radioVar.get()==1):
        resultat = chiffrement_circulaire(texte, step.get())
    else:
        resultat = chiffrement(texte, step.get())

    #Suppression du text afficher dans la zone d'affichage du resultat
    #Affichage de la chaine de caractère dans la zonne d'affichage
    text_resultat.delete('1.0', END)
    text_resultat.insert('1.0', resultat)

#Chiffrement de la chaine de caractère entrrée via le bouton chiffrement 
#De façon circulaire ou non via les radiobutton
def bouton_dechiffrement():
    texte = decoupage(entry.get("0.0", CURRENT))
    if(radioVar.get()==1):
        resultat = dechiffrement_circulaire(texte, step.get())
    else:
        resultat = dechiffrement(texte, step.get())

    #Suppression du text afficher dans la zone d'affichage du resultat
    #Affichage de la chaine de caractère dans la zonne d'affichage
    text_resultat.delete('1.0', END)
    text_resultat.insert('1.0', resultat)

#Création du du bouton de chiffrement  
#Mise en place de la position du bouton
chiffrement_button = Button(frame, text="Chiffrer", command=bouton_chiffrement)
chiffrement_button.grid(row="4", column="2")

#Création du du bouton de déchiffrement  
#Mise en place de la position du bouton
dechiffrement_button = Button(frame, text="Déchiffrer", command=bouton_dechiffrement)
dechiffrement_button.grid(row="4", column="3")

frame.mainloop()



