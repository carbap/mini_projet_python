import string 


#Découpe une chaine de caractère en liste de caractères
#Entrée : str saisie : chaine à découper
#Sortie : Liste de caractères

def decoupage (saisie):
    liste = []
    saisie = str(saisie)
    for character in saisie:
        liste.append(character)
    return liste    

#===================================================================================

#Chiffrement d'une liste de caractère via la méthode Cesar
#Entrées : step : entier , liste : liste[]
#Sortie : crypted_string : string 

def chiffrement (liste, step):
    step = int(step)
    crypted_string = ""

    #Début du chiffrement circulaire via l'alphabet décalé 
    #Récupération du code ASCII du caractère
    #Chiffrement de ce caractère via son code ASCII
    #Création de la nouvelle chaine de caractère chiffrée
    for character in liste:
        value_ascii = ord(character) + step
        crypted_character = chr(value_ascii)
        crypted_string = ''.join([crypted_string, crypted_character])
    return crypted_string
  
#===================================================================================

#Déhiffrement d'une liste de caractère via la méthode Cesar
#Entrées : step : entier , liste : liste[]
#Sortie : crypted_string : string 

def dechiffrement (liste, step):
    step = int(step)
    crypted_string = ""

    #Début du chiffrement circulaire via l'alphabet décalé 
    #Récupération du code ASCII du caractère
    #Déchiffrement de ce caractère via son code ASCII
    #Création de la nouvelle chaine de caractère chiffrée 

    for character in liste:
        value_ascii = ord(character) - step
        crypted_character = chr(value_ascii)
        crypted_string = ''.join([crypted_string,crypted_character])
    return crypted_string

#=====================================================================================

#Chiffrement d'une liste de caractère de façon circulaire via la méthode Cesar
#Entrées : step : entier , liste : liste[]
#Sortie : crypted_string : string 

def chiffrement_circulaire (liste, step):
    step = int(step)
    crypted_string = "" 
    
    #Création d'un alphabet décalé pour faciliter le chiffrement 
    alphabet_maj=decoupage(string.ascii_uppercase)    
    alphabet_min=decoupage(string.ascii_lowercase)
    if(step>26): step = step % 26

    #Mise en place de l'alphabet décalé (Minuscule et Majuscule)
    alphabet_min_crypte = alphabet_min[step:] + alphabet_min[:step]
    alphabet_maj_crypte = alphabet_maj[step:] + alphabet_maj[:step]

    #Début du chiffrement circulaire via l'alphabet décalé 
    #Comparaison de la lettre pour récupérer son index 
    #Chiffreement de la lettre grace à son index et l'alphabet décalé
    #Création de la nouvelle string 
    for character in liste:
        try:
            if(ord(character) > 90):
                index = alphabet_min.index(character)
                crypted_character = alphabet_min_crypte[index]
            else:
                index = alphabet_maj.index(character)
                crypted_character = alphabet_maj_crypte[index]
        except ValueError:
            crypted_character = character
        crypted_string = crypted_string + crypted_character
    return crypted_string

#===================================================================================

#Déchiffrement d'une liste de caractère de façon circulaire via la méthode Cesar
#Entrées : step : entier , liste : liste[]
#Sortie : crypted_string : string 

def dechiffrement_circulaire (liste, step):
    step = int(step)
    crypted_string = ""

    #Création d'un alphabet décalé pour faciliter le chiffrement 
    alphabet_maj=decoupage(string.ascii_uppercase)    
    alphabet_min=decoupage(string.ascii_lowercase)
    if(step>26): step = step % 26

    #Mise en place de l'alphabet décalé (Minuscule et Majuscule)
    alphabet_min_crypte = alphabet_min[-step:] + alphabet_min[:-step]
    alphabet_maj_crypte = alphabet_maj[-step:] + alphabet_maj[:-step]

    #Début du chiffrement circulaire via l'alphabet décalé 
    #Comparaison de la lettre pour récupérer son index 
    #Chiffreement de la lettre grace à son index et l'alphabet décalé
    #Création de la nouvelle string
    
    for character in liste:
        try:
            if(ord(character) > 90):
                index = alphabet_min.index(character)
                crypted_character = alphabet_min_crypte[index]
            else:
                index = alphabet_maj.index(character)
                crypted_character = alphabet_maj_crypte[index]
        except ValueError:
            crypted_character = character
        crypted_string = crypted_string + crypted_character
    return crypted_string    
    